%global _empty_manifest_terminate_build 0
Name:		python-copr
Version:	2.0
Release:	1
Summary:	Python client for copr service.
License:	GPL-2.0-or-later
URL:		https://pagure.io/copr/copr
Source0:	https://files.pythonhosted.org/packages/6e/53/304bdcd531560a626c90566a05c8c3587e3ce53e0d7d56e8ac3839bed0db/copr-2.0.tar.gz
BuildArch:	noarch

%description
Copr is designed to be a lightweight buildsystem that allows contributors
to create packages, put them in repositories, and make it easy for users
to install the packages onto their system. Within the Fedora Project it
is used to allow packagers to create third party repositories.

This part is a python client to the copr service.

%package -n python3-copr
Summary:	Python client for copr service.
Provides:	python-copr = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-copr
Copr is designed to be a lightweight buildsystem that allows contributors
to create packages, put them in repositories, and make it easy for users
to install the packages onto their system. Within the Fedora Project it
is used to allow packagers to create third party repositories.

This part is a python client to the copr service.

%package help
Summary:	Development documents and examples for copr
Provides:	python3-copr-doc
%description help
Copr is designed to be a lightweight buildsystem that allows contributors
to create packages, put them in repositories, and make it easy for users
to install the packages onto their system. Within the Fedora Project it
is used to allow packagers to create third party repositories.

This part is a python client to the copr service.

%prep
%autosetup -n copr-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-copr -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Oct 31 2024 muxiaohui <muxiaohui@kylinos.cn> - 2.0-1
- Update version to 2.0
  Allow admins to set storage for new projects.

* Wed Aug 14 2024 yaoxin <yao_xin001@hoperun.com> - 1.132-1
- Update to 1.132:
  * Discourage from deleting objects while paginating over them
  * Suggest pagination only when a GET request timeouts


* Thu Nov 30 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 1.130-1
- Upgrade package to version 1.130

* Fri Jun 30 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 1.129-1
- Update package to version 1.129

* Wed Apr 12 2023 wubijie <wubijie@kylinos.cn> - 1.128-1
- Update package to version 1.128

* Mon Mar 20 2023 wubijie <wubijie@kylinos.cn> - 1.125-1
- Update package to version 1.125

* Thu Dec 15 2022 liqiuyu <liqiuyu@kylinos.cn> - 1.124-1
- Update package to version 1.124

* Sun Nov 6 2022 hkgy <kaguyahatu@outlook.com> - 1.123-1
- Upgrade to v1.123

* Tue Aug 9 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 1.121-1
- Update to 1.121

* Thu Jun 23 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.119-1
- Upgrade to version 1.119

* Wed Oct 14 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
